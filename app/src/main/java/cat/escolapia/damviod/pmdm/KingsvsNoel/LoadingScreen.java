package cat.escolapia.damviod.pmdm.kingsvsnoel;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Sound;
import cat.escolapia.damviod.pmdm.framework.Music;

/**
 * Created by jonatan.escalera on 21/12/2016.
 */
public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        Graphics g=game.getGraphics();
        Assets.background=g.newPixmap("kingsvsnoel/background.png", Graphics.PixmapFormat.RGB565);
        Assets.backgroundMenu=g.newPixmap("kingsvsnoel/backgroundMenu.png", Graphics.PixmapFormat.RGB565);
        Assets.king1=g.newPixmap("kingsvsnoel/King1.png", Graphics.PixmapFormat.RGB565);
        Assets.king2=g.newPixmap("kingsvsnoel/King2.png", Graphics.PixmapFormat.RGB565);
        Assets.king3=g.newPixmap("kingsvsnoel/King3.png", Graphics.PixmapFormat.RGB565);
        Assets.Noel1=g.newPixmap("kingsvsnoel/Noel1.png", Graphics.PixmapFormat.RGB565);
        Assets.Noel2=g.newPixmap("kingsvsnoel/Noel2.png", Graphics.PixmapFormat.RGB565);
        Assets.mainMenu = g.newPixmap("kingsvsnoel/mainmenu.png", Graphics.PixmapFormat.ARGB4444);
        Assets.credits = g.newPixmap("kingsvsnoel/Credits.png", Graphics.PixmapFormat.ARGB4444);
        Assets.logo = g.newPixmap("kingsvsnoel/logo.png", Graphics.PixmapFormat.ARGB4444);
        Assets.buttons = g.newPixmap("kingsvsnoel/buttons.png", Graphics.PixmapFormat.ARGB4444);
        Assets.numbers = g.newPixmap("kingsvsnoel/numbers.png", Graphics.PixmapFormat.ARGB4444);
        Assets.ready = g.newPixmap("kingsvsnoel/ready.png", Graphics.PixmapFormat.ARGB4444);
        Assets.pause = g.newPixmap("kingsvsnoel/pausemenu.png", Graphics.PixmapFormat.ARGB4444);
        Assets.gameOver = g.newPixmap("kingsvsnoel/gameover.png", Graphics.PixmapFormat.ARGB4444);
        Assets.wellDone = g.newPixmap("kingsvsnoel/WellDone.png", Graphics.PixmapFormat.ARGB4444);
        Assets.camel1=g.newPixmap("kingsvsnoel/camel1.png", Graphics.PixmapFormat.RGB565);
        Assets.camel2=g.newPixmap("kingsvsnoel/camel2.png", Graphics.PixmapFormat.RGB565);
        Assets.camel3=g.newPixmap("kingsvsnoel/camel3.png", Graphics.PixmapFormat.RGB565);
        Assets.kingcamel1=g.newPixmap("kingsvsnoel/kingcamel1.png", Graphics.PixmapFormat.RGB565);
        Assets.kingcamel2=g.newPixmap("kingsvsnoel/kingcamel2.png", Graphics.PixmapFormat.RGB565);
        Assets.kingcamel3=g.newPixmap("kingsvsnoel/kingcamel3.png", Graphics.PixmapFormat.RGB565);

        //Assets.musicMenu = game.getAudio().newMusic("menu.mp3");
        //Assets.musicGame = game.getAudio().newMusic("mario64theme.mp3");

        FileManager.load(game.getFileIO());
        game.setScreen(new MainMenu(game));
    }

    @Override
    public void render(float deltaTime) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
