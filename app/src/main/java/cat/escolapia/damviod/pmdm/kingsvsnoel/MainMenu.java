package cat.escolapia.damviod.pmdm.kingsvsnoel;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by Jonatan on 14/01/2017.
 */

public class MainMenu extends Screen {
    public MainMenu(Game game) {
        super(game);
        //Assets.musicMenu.stop();
        //Assets.musicMenu.isLooping();
    }

    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            Input.TouchEvent event = touchEvents.get(i);
            if(event.type == Input.TouchEvent.TOUCH_UP) {
                if(inBounds(event, 64, 220, 192, 42) ) {
                    game.setScreen(new ReadyGame(game));
                    //Assets.musicMenu.stop();
                    return;
                }
                if(inBounds(event, 64, 220 + 42, 192, 42) ) {
                    game.setScreen(new HighScore(game));
                    return;
                }
                //credits
                if(inBounds(event, 64, 220 + 84, 192, 42) ) {
                    game.setScreen(new Credits(game));
                    return;
                }
                //Sortir
                if(inBounds(event, 64, 220 + 126, 192, 42) ) {
                    System.exit(0);
                    return;
                }
            }
        }
    }

    private boolean inBounds(Input.TouchEvent event, int x, int y, int width, int height) {
        if(event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1)
            return true;
        else
            return false;
    }

    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.backgroundMenu, 0, 0);
        g.drawPixmap(Assets.mainMenu, 64, 220);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        //Assets.musicMenu.play();
        //Assets.musicMenu.setLooping(true);
    }

    @Override
    public void dispose() {

    }
}
