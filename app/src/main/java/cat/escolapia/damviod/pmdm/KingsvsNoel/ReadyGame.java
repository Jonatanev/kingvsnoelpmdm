package cat.escolapia.damviod.pmdm.kingsvsnoel;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by Jonatan on 18/01/2017.
 */

public class ReadyGame extends Screen {
    public static int countDown=5;

    public ReadyGame(Game game) {
        super(game);
        countDown=5;
    }

    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();
        delaySegundo();
        if(countDown==0)
        {
                GameScreen.estado = GameScreen.GameState.Running;
                game.setScreen(new GameScreen(game));
        }
    }

    private boolean inBounds(Input.TouchEvent event, int x, int y, int width, int height) {
        if(event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1)
            return true;
        else
            return false;
    }

    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.ready, 0, 0);
        GameScreen.drawText(g,String.valueOf(countDown),g.getWidth() / 2 , g.getHeight() - 42);
    }

    private static void delaySegundo()
    {
        try{
            Thread.sleep(1000);
            countDown--;
        }catch(InterruptedException e){}
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
