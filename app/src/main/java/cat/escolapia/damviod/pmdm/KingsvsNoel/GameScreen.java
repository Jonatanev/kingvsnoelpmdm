package cat.escolapia.damviod.pmdm.kingsvsnoel;


import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input.*;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Pixmap;

import java.util.List;

/**
 * Created by jonatan.escalera on 21/12/2016.
 */
public class GameScreen extends Screen {
    enum GameState{
        Running, Paused,GameOver, Win
    }
   public static GameState estado=GameState.Running; //Estado con el que empieza al arrancar el juego// .
    World world;
    int movx, movy;

    public GameScreen(Game game) {
        super(game);
        world= new World();
    }

    @Override
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        if(estado==GameState.Running) updateRunning(touchEvents, deltaTime);
        if(estado == GameState.Paused) updatePaused(touchEvents);
        if(estado == GameState.GameOver)  updateGameOver(touchEvents);
        if(estado == GameState.Win)  updateWin(touchEvents);
    }
/*
    private void updateReady( List<TouchEvent> touchEvents)
    {

        if (touchEvents.size() > 0) estado = GameState.Running;

    }
*/

    private void updateRunning(List<TouchEvent> touchEvents, float deltaTime)
    {
        int len = touchEvents.size();
        int dirx, diry;
        int numking=world.numking;
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            ////movimiento King
            switch (event.type) {
                case TouchEvent.TOUCH_DOWN:
                    movx=event.x;
                    movy=event.y;
                    break;
                case TouchEvent.TOUCH_UP:
                    if(event.x > 288 && event.y < 32) {
                        estado = GameState.Paused;
                        world.initTime(false);
                        world.segundosPaused=world.segundos;
                        return;
                    }
                    dirx=event.x-movx;
                    diry=event.y-movy;
                    if(dirx<0 && Math.abs(diry)<Math.abs(dirx))
                    {
                            if(world.kings[numking].x>0) world.kings[numking].x--;
                    }
                    else if(dirx>0 && Math.abs(diry)<Math.abs(dirx) )
                    {
                            if(world.kings[numking].x<9)world.kings[numking].x++;
                    }

                    if(diry<0 && Math.abs(diry)>Math.abs(dirx))
                    {
                            if(world.kings[numking].y>0)world.kings[numking].y--;
                    }
                    else if(diry>0 && Math.abs(diry)>Math.abs(dirx) )
                    {
                            if(world.kings[numking].y<14)world.kings[numking].y++;
                    }
                    break;
            }


        }
        world.update(deltaTime);
        if(world.gameOver) {
            estado = GameState.GameOver;
        }
        if(world.gameWin)
        {
            estado=GameState.Win;
        }
    }

    private void updatePaused(List<TouchEvent> touchEvents)
    {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x > 80 && event.x <= 240) {
                    if(event.y > 100 && event.y <= 148) {
                        estado = GameState.Running;
                        world.initTime(true);
                        world.segundos=world.segundosPaused;
                        return;
                    }
                    if(event.y > 148 && event.y < 196) {
                        game.setScreen(new MainMenu(game));
                        return;
                    }
                }
            }
        }
    }

    private void updateGameOver(List<TouchEvent> touchEvents)
    {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x >= 128 && event.x <= 192 &&
                        event.y >= 200 && event.y <= 264) {
                    game.setScreen(new MainMenu(game));
                    return;
                }
            }
        }
    }

    private void updateWin(List<TouchEvent> touchEvents)
    {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x >= 128 && event.x <= 192 &&
                        event.y >= 200 && event.y <= 264) {
                    game.setScreen(new MainMenu(game));
                    return;
                }
            }
        }
    }
    /*
    public void startSameSave() {
        if (world.kingGoal ==3) {
            Intent intent = new Intent(Application.ApplicationContext,typeof(SaveGame));
            intent.setClass(getApplicationContext(), SaveGame.class);
            startActivity(intent);
        }
    }
    */
    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.background,0,0);
        drawWorld(world);
        if(estado == GameState.Running)
            drawRunningUI();
        if(estado == GameState.Paused)
            drawPausedUI();
        if(estado == GameState.GameOver)
            drawGameOverUI();
        if(estado == GameState.Win)
            drawWinUI();
    }

    private void drawWorld(World world)
    {
        Graphics g = game.getGraphics();
        King king[]=world.kings;
        int numking=world.numking;
        Noel noel[]=world.noel;
        Camel camel[]=world.camels;

        int x, y;
        //King1
        Pixmap king1Pixmap = Assets.king1;
        //King2
        Pixmap king2Pixmap = Assets.king2;
        //King3
        Pixmap king3Pixmap = Assets.king3;
        x=king[numking].x*32;
        y = king[numking].y*32;
        if(numking==0) g.drawPixmap(king1Pixmap,x,y);
        if(numking==1)  g.drawPixmap(king2Pixmap,x,y);
        if(numking==2)g.drawPixmap(king3Pixmap,x,y);

        //Camels
        //camel1
        Pixmap camel1Pixmap = Assets.camel1;
        //King2
        Pixmap camel2Pixmap = Assets.camel2;
        //King3
        Pixmap camel3Pixmap = Assets.camel3;
        for(int i =0; i<camel.length;i++)
        {
            x=camel[i].x*32;
            y=camel[i].y*32;
            if(camel[i].type==1)g.drawPixmap(camel1Pixmap,x,y);
            if(camel[i].type==2)g.drawPixmap(camel2Pixmap,x,y);
            if(camel[i].type==3)g.drawPixmap(camel3Pixmap,x,y);
        }
        //Noeles
        Pixmap noelPixmap = Assets.Noel1;
        Pixmap noelPixmap2 = Assets.Noel2;
        for(int i=0; i<=12;i++)
        {
            if(noel[i].direction==1)
            {
                x = noel[i].x*32;
                y = noel[i].y*32;
                g.drawPixmap(noelPixmap,x,y);
            }else if(noel[i].direction==0)
            {
                x = noel[i].x*32;
                y = noel[i].y*32;
                g.drawPixmap(noelPixmap2,x,y);
            }
        }
    }

    private void drawRunningUI()
    {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.buttons, 288, 0, 82, 145, 32, 32); //Pause button
        //tiempo
        drawText(g, String.valueOf(world.segundos), String.valueOf(world.segundos).length()*20 /2, 0);
    }

    private void drawPausedUI()
    {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.pause, 80, 100);
    }
    ///
    private void drawGameOverUI()
    {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.gameOver, 62, 100);
        g.drawPixmap(Assets.buttons, 128, 200, 0, 128, 64, 64);
    }

    private void drawWinUI()
    {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.wellDone, 0, 0);
        g.drawPixmap(Assets.buttons, 128, 200, 0, 128, 64, 64);
    }

    public static void drawText(Graphics g, String line, int x, int y) {
        int len = line.length();
        for (int i = 0; i < len; i++) {
            char character = line.charAt(i);

            if (character == ' ') {
                x += 20;
                continue;
            }

            int srcX = 0;
            int srcWidth = 0;
            if (character == '.') {
                srcX = 200;
                srcWidth = 10;
            } else {
                srcX = (character - '0') * 20;
                srcWidth = 20;
            }
            g.drawPixmap(Assets.numbers, x, y, srcX, 0, srcWidth, 32);
            x += srcWidth;
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
