package cat.escolapia.damviod.pmdm.kingsvsnoel;

import java.util.Random;

import cat.escolapia.damviod.pmdm.framework.Graphics;

/**
 * Created by Jonatan on 29/12/2016.
 */

public class Noel extends Row {
    public int vel;
    public int direction;
    private Random random = new Random();

    Noel(int _x,int _y, int _vel)
    {
        super(_x,_y);
        vel=_vel;
        direction =random.nextInt(1);
    }
    public void moveNoel()
    {
        if(direction==1 && this.x<9)
        {
            this.x=this.x+this.vel;
            if(this.x>=9) direction=0;
        }
        else if(direction==0 && this.x>0)
        {
            this.x=this.x-this.vel;
            if(this.x<=0) direction=1;
        }
    }
    //Detecta colision con king
    public boolean detectImpact(King k) {
        if(this.x==k.x&& this.y==k.y) return true;
        return false ;
    }

}
