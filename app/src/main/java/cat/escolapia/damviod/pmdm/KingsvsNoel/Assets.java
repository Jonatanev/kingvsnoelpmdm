package cat.escolapia.damviod.pmdm.kingsvsnoel;

import cat.escolapia.damviod.pmdm.framework.Music;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;

/**
 * Created by jonatan.escalera on 21/12/2016.
 */
public class Assets {
    public static Pixmap background;
    public static Pixmap backgroundMenu;
    public static Pixmap king1;
    public static Pixmap king2;
    public static Pixmap king3;
    public static Pixmap Noel1;
    public static Pixmap Noel2;
    public static Pixmap logo;
    public static Pixmap mainMenu;
    public static Pixmap credits;
    public static Pixmap buttons;
    public static Pixmap numbers;
    public static Pixmap ready;
    public static Pixmap pause;
    public static Pixmap gameOver;
    public static Pixmap wellDone;
    public static Pixmap camel1;
    public static Pixmap camel2;
    public static Pixmap camel3;
    public static Pixmap kingcamel1;
    public static Pixmap kingcamel2;
    public static Pixmap kingcamel3;

    public static Music musicMenu;
    public static Music musicGame;

    public static Sound steps;

}
