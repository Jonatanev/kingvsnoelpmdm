package cat.escolapia.damviod.pmdm.kingsvsnoel;

/**
 * Created by jonatan.escalera on 21/12/2016.
 */
public class King {
    public int x;
    public int y;
    public int type;

    King(int _x, int _y,int _type)
    {
        x=_x;
        y=_y;
        type=_type;
    }

    public int checkGoal(Camel camel)
    {
        if(this.x==camel.x && this.y==camel.y){
            if(this.type==camel.type){return 2;}
            else{return 1;}
        }
        return 0;
    }

    public boolean checkNoel(Noel noel)
    {
        if(this.x==noel.x&& this.y==noel.y) return true;
        return false ;
    }


}
