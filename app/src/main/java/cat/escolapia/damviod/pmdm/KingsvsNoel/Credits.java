package cat.escolapia.damviod.pmdm.kingsvsnoel;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by jonatan.escalera on 18/01/2017.
 */

public class Credits extends Screen {
    public Credits(Game game)
    {
        super(game);

    }

    @Override
    public void update(float deltaTime) {
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
            Input.TouchEvent event = touchEvents.get(i);
            if (event.type == Input.TouchEvent.TOUCH_UP) {
                if (event.x < 64 && event.y > 416) {
                    //click.play(1);
                    game.setScreen(new MainMenu(game));
                    return;
                }
            }
        }
    }

    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.credits, 0, 0);
        g.drawPixmap(Assets.buttons, 0, 416, 64, 64, 64, 64);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
