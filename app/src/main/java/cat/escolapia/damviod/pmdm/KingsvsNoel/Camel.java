package cat.escolapia.damviod.pmdm.kingsvsnoel;

/**
 * Created by Jonatan on 18/01/2017.
 */

public class Camel {
    public static final int CAMEL_1=1;
    public static final int CAMEL_2=2;
    public static final int CAMEL_3=3;
    public int x;
    public int y;
    public int type;

    Camel(int _x, int _y, int _type)
    {
        x=_x;
        y=_y;
        type=_type;
    }

}
