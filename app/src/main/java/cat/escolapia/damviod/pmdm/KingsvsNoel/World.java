package cat.escolapia.damviod.pmdm.kingsvsnoel;


import java.util.Random;
import android.util.Log;

import static android.content.ContentValues.TAG;

/**
 * Created by jonatan.escalera on 21/12/2016.
 */

public class World {

    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 14;
    static final float TICK_INITIAL = 0.5f;

    public King kings[];
    public Noel noel[];
    public Camel camels[];

    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;
    public boolean gameOver = false;
    public boolean gameWin=false;
    public int numking=0;
    public int kingGoal;
    //public Timer timer;
    public int segundos;
    public int segundosPaused;
    boolean iniTimer; //variable para controlar que empiese a contar la primera vez que entra al gameloop
    private Thread t;


    public World()
    {
        kings=createKing();
        camels=createCamel();
        noel=createRows();
        segundos=0;
        kingGoal=0;
        iniTimer=true;
        initTime(true);
    }
    //Iniciamos Hilo con delay 1 seg, que incrementa variable segundos, que se usará parabuscar en assets buttons.
    public void initTime(final boolean isInterrup)
    {
        t = new Thread() {
            @Override
            public void run() {
                if(isInterrup) {
                    while (!isInterrupted()) {
                        try {
                            Thread.sleep(1000); //1000ms = 1 sec
                            segundos++;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
    }

    King[] createKing()
    {
        King king[]=new King[3];
        int x = WORLD_WIDTH/2;
        int y = WORLD_HEIGHT;
        int type=1;
        for(int i=0;i<king.length;i++,type++)
        {
            king[i] = new King(x,y,type);
        }
        return king;
    }
    Camel[] createCamel()
    {
        Camel camel[]= new Camel[3];
        int x=3;
        int y=0;
        int type=1;
        for(int i=0;i<camel.length;i++,type++)
        {
            camel[i]=new Camel(x,y,type);
            x=x+2;
        }
        return camel;
    }

    Noel[] createRows()
    {
        Noel noel1[]=new Noel[13];
        int x, y=0, vel;
        for(int i=0; i<=12;i++)
        {
            //vel=random.nextInt(3);
            //if(vel==0) vel=1;
            vel=1;
            x=random.nextInt(WORLD_WIDTH);
            if(x==0) x=1;
            y++;
            noel1[i]= new Noel(x,y,vel);
        }

        return noel1;
    }

    public void update(float deltaTime)
    {
        if (gameOver){
            //guardar el tiempo
            return;
        }
        tickTime += deltaTime;
        //tickSecond+=deltaTime;

        //inicio el contador la primera vez que entra al loopgame
        if(iniTimer) {
            t.start();
            iniTimer=false;
        }

        while (tickTime > tick)
        {
            tickTime -= tick;

            //Inteligencia de los Noeles
            checkNoels();

            //Inteligencia de los Kings
            // Kings llegan a meta
            kingGoal();
        }
        Log.d(TAG,String.valueOf(segundos));
        //Log.d(TAG,String.valueOf(time));
    }

    public boolean checkKings(Noel noel)
    {
        boolean impact;
        impact=kings[numking].checkNoel(noel);
        return impact;
    }

    public void checkNoels()
    {
        for (int i = 0; i < noel.length; i++) {
            noel[i].moveNoel();

            if(numking==2)
            {
                if(checkKings(noel[i]))
                {
                    gameOver = true;
                    return;
                }
                if(noel[i].detectImpact(kings[numking]))
                {
                    gameOver = true;
                    return;
                }
            }else if(noel[i].detectImpact(kings[numking]))
            {
                numking++;
            }
            if(checkKings(noel[i])) numking++;
        }
    }

    public void kingGoal()
    {
        for(int i=0; i<camels.length;i++)
        {
            if(kings[numking].checkGoal(camels[i])==2)
            {
                if(numking==2) //Si es el ultimo king
                {
                    //has ganado
                    kingGoal++;
                    gameWin=true;
                    return;
                }else{
                    numking++;
                    kingGoal++;
                }
                //Cambiar Assets.kingcamel1
                goaltoCamel(i);
                return;
            }
            else if(kings[numking].checkGoal(camels[i])==1 ){
                reponerKing();
                return;
            }/*
            else if(kings[numking].checkGoal(camels[i])!=1 && kings[numking].y==0)
            {
                reponerKing();
            }*/

        }

    }

    private void reponerKing()
    {
        kings[numking].x = WORLD_WIDTH/2;
        kings[numking].y = WORLD_HEIGHT;
    }

    private void goaltoCamel(int i)
    {
        if(i==0) Assets.camel1=Assets.kingcamel1;
        if(i==1) Assets.camel2=Assets.kingcamel2;
        if(i==2) Assets.camel3=Assets.kingcamel3;
    }



}


